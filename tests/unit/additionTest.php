<?php

class AdditionTest extends \PHPUnit_Framework_TestCase{



    /** @test */
    public function shouldAddTwoNumbers(){
        $addition = new \App\Calculator\Addition;

        $addition->setOperands([5, 10]);

        $result = $addition->calculate();

        $this->assertEquals(15, $result);
    }

}