<?php
/**
 * Created by PhpStorm.
 * User: jsalisbury
 * Date: 11/12/2018
 * Time: 12:34 PM
 */

namespace App\Calculator;


interface Operations{

    public function calculate();

}