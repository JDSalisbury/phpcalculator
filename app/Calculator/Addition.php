<?php
/**
 * Created by PhpStorm.
 * User: jsalisbury
 * Date: 11/12/2018
 * Time: 12:18 PM
 */

namespace App\Calculator;


class Addition implements Operations{

    protected $operands;

    public function setOperands(array $operands){
        $this->operands = $operands;
    }


    public function calculate(){
//        $result = 0;
//        foreach ($this->operands as $operand){
//            $result += $operand;
//        }
//        return $result;
        return array_sum($this->operands);
   }


}